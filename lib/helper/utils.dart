import 'dart:convert';
import 'dart:math';
import 'package:cebuwonder/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Utils {

    static Map<String, BitmapDescriptor> iconMap = {};

    static final Random _random = Random.secure();

    static String createCryptoRandomString() {
      int length  = 32;
      var values = List<int>.generate(length, (i) => _random.nextInt(256));
      return base64Url.encode(values);
    }

    static IconData getItemIcon(String type) {
      switch (type) {
        case Strings.RESTAURANT:
          return ItemIcons.iconRestaurant;
          break;
        case Strings.HOTEL:
          return ItemIcons.iconHotel;
          break;
        case Strings.SHOP:
          return ItemIcons.iconShop;
          break;
        case Strings.BEAUTY:
          return ItemIcons.iconBeauty;
          break;
        case Strings.SCHOOL:
          return ItemIcons.iconSchool;
          break;
        case Strings.EVENT:
          return ItemIcons.iconEvent;
          break;
        default:
          return ItemIcons.iconRestaurant;
      }
    }

}