import 'dart:ui';

import 'package:flutter/material.dart';

class Strings {
  static const String APP_TITLE = 'CebuWonder';
  static const String TAB_HOME = 'Home';
  static const String TAB_NEARBY = 'Nearby';

  static const String RESTAURANT = 'Restaurant';
  static const String HOTEL = 'Hotel';
  static const String SHOP = 'Shop';
  static const String BEAUTY = 'Beauty';
  static const String SCHOOL = 'School';
  static const String EVENT = 'Event';
  static const String NONE = '';

  static const String NO_DATA = 'No Data Found';
  static const String SHARE_CEBUWONDER = 'Share CebuWonder';
}

class Links {
  static const String ANDROID_LINK = 'https://play.google.com/store/apps/details?id=com.lasapps.cebuwonder';
  static const String IOS_LINK = 'https://play.google.com/store/apps/details?id=com.lasapps.cebuwonder';

  static const String ITEM_URL = 'https://mjmon.github.io/cebuwonder_json/cebuwonder/newjson/newitems.json';
}

class Keys {
  static const String SECRET_KEY = '\$2a\$10\$J5KsGTaCCsnPCXLRhSwa/O8WTexkb/UIFRMetW0HS2uw1DzPAeKim';
}

class HexColors {
  static const Color Primary = Color(0xff75c7ea);
  static const Color PrimaryLight = Color(0xffa9faff);
  static const Color PrimaryDark = Color(0xff3f96b8);
}

class ItemIcons {
  static IconData iconRestaurant = Icons.restaurant_menu;
  static IconData iconHotel = Icons.hotel;
  static IconData iconShop = Icons.shopping_cart;
  static IconData iconBeauty = Icons.face;
  static IconData iconSchool = Icons.school;
  static IconData iconEvent = Icons.event;
}

class ImagesLocation{
  static const String restaurant = 'assets/images/restaurant.png';
  static const String hotel = 'assets/images/hotel.png';
  static const String shop = 'assets/images/shop.png';
  static const String beauty = 'assets/images/beauty.png';
  static const String school = 'assets/images/school.png';
  static const String event = 'assets/images/event.png';
}


