import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';


class ContentBloc extends BlocBase{

  ContentBloc();

  var contentController = BehaviorSubject<int>.seeded(0);

  Stream<int> get contentStream => contentController.stream;
  Sink<int> get contentSink => contentController.sink;

  tabChanged(int index) {
    contentSink.add(index);
  }

  @override
  void dispose() {
    contentController.close();
    super.dispose();
  }

}
