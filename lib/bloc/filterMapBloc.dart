import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cebuwonder/bloc/filterMapProvider.dart';
import 'package:cebuwonder/helper/constants.dart';
import 'package:rxdart/rxdart.dart';

class FilterMapBloc extends BlocBase{

 FilterMapBloc();

 FilterMapProvider provider = FilterMapProvider();

 var filterMapController = BehaviorSubject<Map<String, bool>>.seeded(
   {
      Strings.RESTAURANT: false,
      Strings.HOTEL: false,
      Strings.SHOP: false,
      Strings.BEAUTY: false,
      Strings.SCHOOL: false,
      Strings.EVENT: false,
    }
  );


  Stream<Map<String, bool>> get filterMapStream => filterMapController.stream.asBroadcastStream();
  Sink<Map<String, bool>> get filterMapSink => filterMapController.sink;

  void filterTrigger(String key, bool value) {
    filterMapSink.add(provider.filterTriggered(key, value));
  }

  initData(Map<String, bool> initData) {
    filterMapSink.add(provider.setData(initData));
  }

  @override
  void dispose() {
     filterMapController.close();
     super.dispose();
  }


}