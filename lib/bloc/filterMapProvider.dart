import 'dart:core';
import 'package:cebuwonder/helper/constants.dart';

class FilterMapProvider {

  Map<String, bool> filterMap;
  //  = {
  //     Strings.RESTAURANT: false,
  //     Strings.HOTEL: false,
  //     Strings.SHOP: false,
  //     Strings.BEAUTY: false,
  //     Strings.SCHOOL: false,
  //     Strings.EVENT: false,
  // };

  Map<String, bool> getCurrentFilter() {
    return filterMap;
  }

  Map<String, bool> setData(Map<String, bool> initData) {
    filterMap = initData;
    return filterMap;
  }

  Map<String, bool> filterTriggered(String key, bool value) {
    filterMap[key] = value;
    return filterMap;
  }

}