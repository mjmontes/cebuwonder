import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cebuwonder/bloc/itemProvider.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:rxdart/rxdart.dart';


class ItemListBloc extends BlocBase {

ItemListBloc();

ItemProvider provider = ItemProvider();
//Stream that receives a number and changes the count;
var _listController = BehaviorSubject<List<Item>>.seeded([
]);

Stream<List<Item>> get listStream => _listController.stream;
Sink<List<Item>> get listSink => _listController.sink;

addToList(Item item) {
  listSink.add(provider.addToList(item));
}

addListItems(List<Item> items) {
  listSink.add(provider.addListItems(items));
}

//dispose will be called automatically by closing its streams
@override
void dispose() {
  _listController.close();
  super.dispose();
}

}