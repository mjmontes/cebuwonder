import 'package:cebuwonder/model/item.dart';

class ItemProvider {

  List<Item> listItems = [];

  List<Item> getAllList() {
    return listItems;
  }

  List<Item> addToList(Item item) {
    listItems.add(item);
    return listItems;
  }

  List<Item> addListItems(List<Item> items) {
    listItems = items;
    return listItems;
  }

}