import 'dart:convert';
import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class MyProvider with ChangeNotifier{

  List<Item> _items = List<Item>();
  bool _isFetching = false;

  List<Item> get items => _items;
  List<Item> get featuredItems => _items.where((f) => f.featured).toList();
  bool get isFetching => _isFetching;

  MyProvider() {
  }

  void fetchData() async {
    print('Fetching Data!!!!!!!!');
    _isFetching = true;
    notifyListeners();

    var response = await http.get(
        Links.ITEM_URL,
      );
     Iterable mIterable = json.decode(response.body);
     _items = mIterable.map((f) => Item.fromJson(f)).toList();

    _isFetching = false;
    notifyListeners();
  }

}