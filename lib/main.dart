import 'dart:convert';
// import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cebuwonder/bloc/contentBloc.dart';
import 'package:cebuwonder/bloc/filterMapBloc.dart';
import 'package:cebuwonder/bloc/itemListBloc.dart';
import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/pages/tabs/home.dart';
import 'package:cebuwonder/pages/tabs/nearby.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:io' show Platform;
import 'package:package_info/package_info.dart';
import 'package:flutter/services.dart';
import 'model/item.dart';
import 'package:http/http.dart' as http;
import 'globals.dart' as globals;
import 'package:provider/provider.dart';
import 'provider/myprovider.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: HexColors.Primary,
    statusBarColor: HexColors.Primary,
  ));
  runApp(
     MaterialApp(
       debugShowCheckedModeBanner: false,
       home: SplashPage(),
     )
  );
}

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: new MyApp(),
      image: Image.asset('assets/splash/splash_image.png'),
      gradientBackground: new LinearGradient(colors: [Colors.cyan, Colors.blue], begin: Alignment.topLeft, end: Alignment.bottomRight),
      // backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      loaderColor: HexColors.PrimaryLight,
    );
  }
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
          blocs: [
            Bloc((i) => ItemListBloc()),
            Bloc((i) => ContentBloc()),
            Bloc((i) => FilterMapBloc()),
          ],
          child: MaterialApp(
            title: Strings.APP_TITLE,
            debugShowCheckedModeBanner: false,
            theme:
            ThemeData(
              primaryColor: HexColors.Primary,
              primaryColorDark: HexColors.PrimaryDark,
              primaryColorLight: HexColors.PrimaryLight,
              primaryIconTheme: IconThemeData(color: Colors.white),
              primaryTextTheme: TextTheme(title: TextStyle(color: Colors.white, fontSize: 18.0))
            ),
            home: ChangeNotifierProvider<MyProvider>(
              builder: (context) => MyProvider(),
              child: MyHomePage(),
            ),
          )
        );

  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage();
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final ItemListBloc itemListBloc = BlocProvider.getBloc<ItemListBloc>();
  final ContentBloc contentBloc = BlocProvider.getBloc<ContentBloc>();

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey();
  List<Item> mItems = List<Item>();

  @override
  void initState() {
    super.initState();
    Provider.of<MyProvider>(context, listen: false).fetchData();
    getItems();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  getItems() async {
    var response = await http.get(
        Links.ITEM_URL,
      );
     Iterable mIterable = json.decode(response.body);
     mItems = mIterable.map((f) => Item.fromJson(f)).toList();
     itemListBloc.addListItems(mItems);
  }

  @override
  Widget build(BuildContext context) {
    final mState = Provider.of<MyProvider>(context);

    return SafeArea(
        child:Scaffold(
            key: scaffoldKey,
            appBar: CustomAppbar(scaffoldKey),
            drawer: CustomDrawer(),
            body:
              mState.isFetching?
              Container(child: Center(child: CircularProgressIndicator(),),)
              : StreamBuilder(
                  stream: contentBloc.contentStream,
                  builder: (context, snapshot) {
                    switch (snapshot.data) {
                      case 0:
                        return Home();
                      case 1:
                        return Nearby();
                      default:
                        return Home();
                    }
                  },
                ),
              bottomNavigationBar: bottomNavBar(),
            ),
          );

}


Widget bottomNavBar() {
  return FancyBottomNavigation(
    tabs: <TabData>[
      TabData(iconData: Icons.home,title: 'Home'),
      TabData(iconData: Icons.place,title: 'Nearby'),
    ],
    onTabChangedListener: (int position) {
      contentBloc.tabChanged(position);
    },
  );
}
}

class CustomAppbar extends StatelessWidget with PreferredSizeWidget{

  final GlobalKey<ScaffoldState> scaffoldKey;
  CustomAppbar(this.scaffoldKey);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(Icons.menu, color: HexColors.Primary,),
        padding: EdgeInsets.all(0),
        onPressed: () {
          scaffoldKey.currentState.openDrawer();
        })
      );
  }
  @override
  Size get preferredSize => Size.fromHeight(30);
}

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key key}) : super(key: key);
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {

  String appName = '';
  String packageName = '';
  String version = '';
  String buildNumber = '';

  @override
  void initState(){
    getPackageInfo();
    super.initState();
  }

  getPackageInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      appName = packageInfo.appName;
      packageName = packageInfo.packageName;
      version = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
    });

  }

  @override
  Widget build(BuildContext context) {

    String qrLink = Links.ANDROID_LINK;
    if(Platform.isIOS) {
      qrLink = Links.IOS_LINK;
    }

    return Drawer(
      child: SafeArea(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              height: 50,
              color: Colors.blue,
              child: Text('$appName v$version', style: TextStyle(fontSize: 15, color: Colors.white),),
            ),
            ListTile(
              title: Text(Strings.SHARE_CEBUWONDER),
              subtitle: QrImage(
                data: qrLink,
                size: 200.0,
              ),
            ),
          ],
        ),
      )
    );
  }
}