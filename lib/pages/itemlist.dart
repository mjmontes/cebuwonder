import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cebuwonder/bloc/itemListBloc.dart';
import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/helper/utils.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:cebuwonder/pages/itemdetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ItemList extends StatefulWidget {

  final String type;
  ItemList(this.type);

  _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(widget.type),
      body: ListContent(widget.type)
    );
  }
}

class CustomAppbar extends StatelessWidget with PreferredSizeWidget{

  final String type;
  CustomAppbar(this.type);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton( icon: Icon(CupertinoIcons.back),
        onPressed: () {
          Navigator.of(context).pop();
      },),
      title: Text(type, style: TextStyle(color: Colors.white)),
      actions: <Widget>[
        IconButton( icon: Icon(CupertinoIcons.search),
        onPressed: () {
          print('Search Pressed');
          showSearch(context: context, delegate: DataSearch(type));
        },
        )
      ],
    );
  }
  @override
  Size get preferredSize => Size.fromHeight(50);
}


class DataSearch extends SearchDelegate{
  String type;
  DataSearch(this.type);

  final ItemListBloc bloc = BlocProvider.getBloc<ItemListBloc>();
  List<Item> mList;



  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(icon: Icon(Icons.clear), onPressed: () {
        print('Clear Pressed!');
        query = '';
      },)
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    print('Build Results: $query');

    return (query.isEmpty)?
    Container(
      child: Center(child: Text('Empty Results'),),
    ):
    StreamBuilder(
      stream: bloc.listStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        final List<Item> resultList = mList.where((f) => (f.name.toLowerCase().contains(query.toLowerCase()))).toList();
        return Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: resultList.length,
                itemBuilder: (context, index) {
                  return ItemContent(resultList.elementAt(index), index);
                },
              ),
            )
          ]
        );
      },
    );

  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return StreamBuilder(
      stream: bloc.listStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.data != null) {
          mList = ((snapshot.data) as List<Item>).where((q) => q.type == type).toList();
          final suggestionList = (query.isEmpty)? mList: (mList.where((f)=> (f.name.toLowerCase().contains(query.toLowerCase()))).toList());
          return
            Column(
              children: [
                  Expanded(
                    child: ListView.builder(
                      itemCount: suggestionList.length,
                      itemBuilder: (context, index) {
                        return ListTile(

                          leading: Icon(Utils.getItemIcon(type), color: HexColors.Primary, size: 30,),
                          title: Text(suggestionList.elementAt(index).name),
                          onTap: () {
                            // showResults(context);
                            Navigator.push(
                              context,
                              CupertinoPageRoute(builder: (_context) => ItemDetails(suggestionList.elementAt(index)))
                            );
                          },
                        );
                      },
                    ),
                  )
                ]
            );
        }
        else {
          return Container(
            child: Center(
              child: Text('No Data Found'),
            )
          );
        }
      },
    );
  }

}


class ListContent extends StatefulWidget {
  final String type;
  ListContent(this.type);
  _ListContentState createState() => _ListContentState();
}

class _ListContentState extends State<ListContent> {
  final ItemListBloc bloc = BlocProvider.getBloc<ItemListBloc>();
  List<Item> mList;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: bloc.listStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.data != null) {
          mList = ((snapshot.data) as List<Item>).where((q) => q.type == widget.type).toList();
          return
            Column(
              children: [
                  Expanded(
                    child: ListView.builder(
                      itemCount: mList.length,
                      itemBuilder: (context, index) {
                        return ItemContent(mList.elementAt(index), index);
                      },
                    ),
                  )
                ]
            );
        }
        else {
          return Container(
            child: Center(
              child: Text('No Data Found'),
            )
          );
        }
      },
    );
  }
}


class ItemContent extends StatelessWidget {
  final Item mItem;
  final int mIndex;
  ItemContent(this.mItem, this.mIndex);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('Item $mIndex Tapped');
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (_context) => ItemDetails(mItem))
        );
      },
      child: Card(
      elevation: 2,
      child: Container(
        height: 150,
        child:
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 150,
                    child: Padding(
                      padding: EdgeInsets.all(5),
                      child:
                      (mItem.hasPromo)?
                      Banner(
                        color: Colors.orange,
                        message: 'Promo',
                        location: BannerLocation.topEnd,
                        child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl: mItem.image.elementAt(0),
                            placeholder: (context, url) => Container( child: Center(child: new CircularProgressIndicator(),),),
                            errorWidget: (context, url, error) => Container(child: Center(child: Icon(Icons.error),),),
                        ),
                      )
                      : CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl: mItem.image.elementAt(0),
                            placeholder: (context, url) => Container( child: Center(child: new CircularProgressIndicator(),),),
                            errorWidget: (context, url, error) => Container(child: Center(child: Icon(Icons.error),),),
                      ),
                      // Image.network('${mItem.image.elementAt(0)}', alignment: Alignment.topLeft,),
                    )
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.all(5),
                      child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('${mItem.name}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12), overflow: TextOverflow.ellipsis,),
                        Row(children: <Widget>[
                          Icon((((mItem.rating>1.0)||(mItem.rating==1.0))?Icons.star: ((mItem.rating>0)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                          Icon((((mItem.rating>2.0)||(mItem.rating==2.0))?Icons.star: ((mItem.rating>1)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                          Icon((((mItem.rating>3.0)||(mItem.rating==3.0))?Icons.star: ((mItem.rating>2)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                          Icon((((mItem.rating>4.0)||(mItem.rating==4.0))?Icons.star: ((mItem.rating>3)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                          Icon((((mItem.rating>5.0)||(mItem.rating==5.0))?Icons.star: ((mItem.rating>4)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                          Text('${mItem.rating}', style: TextStyle(fontWeight: FontWeight.bold),),
                        ],),
                        SizedBox(height: 5,),
                        Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Icon(Icons.location_on),Expanded(child: Text('${mItem.address}', maxLines: 3, overflow: TextOverflow.ellipsis,),)],),
                      ],
                    ),
                    )
                  ),
                )
              ],
            ),
    ),
    ),
    );
  }
}

