import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cebuwonder/bloc/filterMapBloc.dart';
import 'package:cebuwonder/bloc/itemListBloc.dart';
import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/helper/utils.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:cebuwonder/pages/itemdetails.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class Nearby extends StatefulWidget {
  Nearby({Key key}) : super(key: key);
  _NearbyState createState() => _NearbyState();
}

class _NearbyState extends State<Nearby> {

  final FilterMapBloc filterMapBloc = BlocProvider.getBloc<FilterMapBloc>();

  @override
  void initState() {

    var initMapData = {
      Strings.RESTAURANT: true,
      Strings.HOTEL: true,
      Strings.SHOP: true,
      Strings.BEAUTY: true,
      Strings.SCHOOL: true,
      Strings.EVENT: true,
    };

    filterMapBloc.initData(initMapData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          MapStuff(),
          OverlapContent()
        ],
      ),
    );
  }
}


class MapStuff extends StatefulWidget {
  MapStuff({Key key}) : super(key: key);
  _MapStuffState createState() => _MapStuffState();
}

class _MapStuffState extends State<MapStuff> {
  int index = 1;
  final FilterMapBloc filterMapBloc = BlocProvider.getBloc<FilterMapBloc>();
  final ItemListBloc itemListBloc =  BlocProvider.getBloc<ItemListBloc>();

  bool hasLocation = false;
  Location mLocation = Location();
  LatLng currentLocation = LatLng(10.30539, 123.89784);
  // LatLng currentLocation = LatLng(0, 0);
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> markers = {};
  Map<String, BitmapDescriptor> iconMap = {};

  @override
  void initState() {
    getCustomMarkers();
    checkLocationService();
    // checkLocationPermission();
    // askEnablePermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return IndexedStack(
      index: index,
      children: <Widget>[
        StreamBuilder(
        stream: filterMapBloc.filterMapStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.data != null) {
            Map<String, bool> filterMap = (snapshot.data as Map<String, bool>);
            print("*******\nRestaurant: ${filterMap[Strings.RESTAURANT]}\nHotel: ${filterMap[Strings.HOTEL]}\nShop: ${filterMap[Strings.SHOP]}\nBeauty: ${filterMap[Strings.BEAUTY]}\nSchool: ${filterMap[Strings.SCHOOL]}\nEvent: ${filterMap[Strings.EVENT]}\n",);
            return
            // (hasLocation)?
              StreamBuilder(
                stream: itemListBloc.listStream,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  List<Item> data = (snapshot.data as List<Item>).where((q) => (filterMap[q.type] == true)).toList();
                  markers.clear();
                  data.forEach((f) {
                    markers.add(Marker(
                      icon: iconMap[f.type],
                      markerId: MarkerId(f.id),
                      position: LatLng(f.coordinates['lat'], f.coordinates['lng']),
                      infoWindow: InfoWindow(title: f.name),
                      onTap: () {
                        print('${f.name} Pressed!');
                        showBottomSheet(context, f);
                      }
                    ));
                  });
                  return GoogleMap(
                    myLocationEnabled: true,
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(target: currentLocation, zoom: 13),
                    onMapCreated: (GoogleMapController controller) async {
                      _controller.complete(controller);
                      print('hasLocation: $hasLocation');
                      print('fetched location: ${currentLocation.latitude} ${currentLocation.longitude}');
                      final GoogleMapController mController = await _controller.future;
                      mController.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(target: LatLng(currentLocation.latitude, currentLocation.longitude), zoom: 13)));
                      await Future.delayed(Duration(milliseconds: 600));
                      setState(() {
                      index = 0;
                      });
                    },
                    markers: markers,
                 );
                },
              );
              // :CircleLoad();
          }
          else{
            return CircleLoad();
          }
        },
        ),

      ],
    );
  }

  void getCustomMarkers() {
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.restaurant).then((f) => iconMap[Strings.RESTAURANT]=f);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.hotel).then((f) => iconMap[Strings.HOTEL]=f);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.shop).then((f) => iconMap[Strings.SHOP]=f);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.beauty).then((f) => iconMap[Strings.BEAUTY]=f);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.school).then((f) => iconMap[Strings.SCHOOL]=f);
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(20, 20)), ImagesLocation.event).then((f) => iconMap[Strings.EVENT]=f);
  }

 void showBottomSheet(BuildContext context,Item mItem) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) => MyBottomSheet(item: mItem,)
    );
 }

 void checkLocationService() {
    mLocation.serviceEnabled().then((status) {
      checkLocationServiceStatus(status);
    });
  }
  void checkLocationServiceStatus(bool status) {
    if(status == true) {
        //checkPermission here...
        checkLocationPermission();
      }
      else { // ask to enable gps
        askEnableService();
      }
  }

  void askEnableService() {
    mLocation.requestService().then((status) {
      checkLocationServiceStatus(status);
    });
  }
  void checkLocationPermission() {
    mLocation.hasPermission().then((status) {
      checkLocationPermissionStatus(status);
    });
  }
  void checkLocationPermissionStatus(bool status) {
    if(status == true) { //Has location permission <3
      getCurrentLocation();
    }
    else { //if no location permission, ask
        askEnablePermission();
    }
  }
  void askEnablePermission() {
    mLocation.requestPermission().then((status) {
      checkLocationPermissionStatus(status);
    });
  }
  void getCurrentLocation() {
    var location = new Location();
      location.getLocation().then((loc) async {
        print("current latitude: ${loc.latitude}");
        print("current longitude: ${loc.longitude}");
        setState(() {
            hasLocation = true;
            currentLocation = LatLng(loc.latitude, loc.longitude);
        });
      });
  }
}


class MyBottomSheet extends StatelessWidget {
  final Item item;
  const MyBottomSheet({Key key, this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(18)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(item.name, style: TextStyle(fontSize: 18, color: HexColors.PrimaryDark), overflow: TextOverflow.ellipsis,),
          SizedBox(height: 2,),
          ImageGallery(item.image),
          SizedBox(height: 10,),
          SizedBox(height: 2,),
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Icon(Icons.location_on),Expanded(child: Text('${item.address}', style: TextStyle(fontSize: 15), maxLines: 2, overflow: TextOverflow.ellipsis,),)],),
          SizedBox(height: 4,),
          RichText(
            text: TextSpan(
              style:  TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                 TextSpan(text: 'Opening Hours: ', style:  TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: item.openingHours, style: TextStyle(fontSize: 15, color: Colors.green)),
              ],
            ),
          ),
          SizedBox(height: 4,),
          InkWell(
            child: Container(child: Center(child: Text('See More...', style: TextStyle(fontSize: 15, color: HexColors.PrimaryDark),),),),
            onTap: () {
              Navigator.push(
                context,
                CupertinoPageRoute(builder: (_context) => ItemDetails(item))
              );
            },
          )
        ],
      ),
    );
  }
}

class ImageGallery extends StatelessWidget {
  final List<String> images;
  ImageGallery(this.images);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(images.length, (index) {
          return ImageContent(images.elementAt(index));
          // return Placeholder(fallbackHeight: 100, fallbackWidth: 100,);
        })
      ),
    );
  }
}

class ImageContent extends StatelessWidget {
  final String image;
  ImageContent(this.image);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 5),
      height: 80,
      width: 120,
      child: CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: image,
        placeholder: (context, url) => Container( color: Colors.grey[300], height: 120, width: 80, child: Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),)),
        errorWidget: (context, url, error) => Container( height: 120, width: 80, child: Center(child: Icon(Icons.error),),),
      ),
    );
  }
}



class CircleLoad extends StatelessWidget {
  const CircleLoad({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}



class OverlapContent extends StatefulWidget {
  OverlapContent({Key key}) : super(key: key);
  _OverlapContentState createState() => _OverlapContentState();
}

class _OverlapContentState extends State<OverlapContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 20, 15, 0),
       child: Column(
                  children: <Widget>[
                    MapSearch(),
                    SizedBox(height: 30,),
                    MapFilter(),
                  ],
                ),
    );
  }
}


class MapSearch extends StatefulWidget {
  MapSearch({Key key}) : super(key: key);
  _MapSearchState createState() => _MapSearchState();
}

class _MapSearchState extends State<MapSearch> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: false,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25)
        ),
        elevation: 2,
        child: TextField(
          textAlign: TextAlign.center,
          decoration: InputDecoration(
            suffixIcon: Icon(Icons.search),
            border: InputBorder.none,
            hintText: 'Search...',
            contentPadding: EdgeInsets.fromLTRB(20, 5, 0, 5)
          ),
        ),
      ),
    );
  }
}


class MapFilter extends StatefulWidget {
  const MapFilter({Key key}) : super(key: key);
  @override
  _MapFilterState createState() => _MapFilterState();
}

class _MapFilterState extends State<MapFilter> {

  final FilterMapBloc filterMapBloc = BlocProvider.getBloc<FilterMapBloc>();

  Map<String, bool> filterMap;

  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15)
      ),
      child:

          StreamBuilder(
            stream: filterMapBloc.filterMapStream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if(snapshot.data != null) {
                filterMap = snapshot.data;

                return  Container(
            height: 100,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.RESTAURANT],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.RESTAURANT, value);
                          },
                          ),
                          Text(Strings.RESTAURANT, style: TextStyle(fontSize: 12),)
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.BEAUTY],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.BEAUTY, value);
                          },
                          ),
                          Text(Strings.BEAUTY, style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ],
                    ),
                  )
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.HOTEL],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.HOTEL, value);
                          },
                          ),
                          Text(Strings.HOTEL, style: TextStyle(fontSize: 12),)
                        ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.SCHOOL],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.SCHOOL, value);
                          },
                          ),
                          Text(Strings.SCHOOL, style: TextStyle(fontSize: 12),)
                        ],
                        ),
                      ],
                    ),
                  )
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.SHOP],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.SHOP, value);
                          },
                          ),
                          Text(Strings.SHOP, style: TextStyle(fontSize: 12),)
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Checkbox(
                          value: filterMap[Strings.EVENT],
                          onChanged: (bool value) {
                            filterMapBloc.filterTrigger(Strings.EVENT, value);
                          },
                          ),
                          Text(Strings.EVENT, style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ],
                    ),
                  )
                ),
              ],
            ),
          );
              }
              else{
                return Container(child: Center(child: CircularProgressIndicator(),));              }
            },
          )
      );
  }
}

