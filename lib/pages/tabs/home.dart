import 'dart:convert';
import 'dart:core';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cebuwonder/bloc/itemListBloc.dart';
import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:cebuwonder/pages/itemdetails.dart';
import 'package:cebuwonder/pages/itemlist.dart';
import 'package:cebuwonder/provider/myprovider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart' as http;
// import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:provider/provider.dart';


class Home extends StatefulWidget {
  Home();
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
              child: ListView(
                children: <Widget>[
                  Featured(),
                  GridMenu(),
                  TopContent(),
                ],
              )
          );
  }
}


class Featured extends StatefulWidget {

  Featured();

  @override
  _FeaturedState createState() => _FeaturedState();
}

class _FeaturedState extends State<Featured> {
  // final ItemListBloc itemListBloc = BlocProvider.getBloc<ItemListBloc>();


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final mState = Provider.of<MyProvider>(context);

    return Container(
      margin: EdgeInsets.only(top: 5),
      height: 200,
      child: Swiper(
            autoplay: true,
            itemCount: mState.featuredItems.length,
            viewportFraction: 0.8,
            scale: 0.9,
            loop: true,
            pagination: SwiperPagination(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.all(30),
              builder: SwiperPagination.dots,
            ),
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  CachedNetworkImage(
                    height: 180,
                    fit: BoxFit.fitWidth,
                    imageUrl: mState.featuredItems.elementAt(index).image.elementAt(0),
                    placeholder: (context, url) => Container( color: Colors.grey[300], height: 180, child: Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),)),
                    errorWidget: (context, url, error) => Container( height: 180, child: Center(child: Icon(Icons.error),),),
                  ),
                  Text(mState.featuredItems.elementAt(index).name, style: TextStyle(fontSize: 15),textAlign: TextAlign.start, overflow: TextOverflow.ellipsis,)
                ],
                ),
                onTap: () {
                  Navigator.push(context,
                    CupertinoPageRoute(builder: (_context) => ItemDetails(mState.featuredItems.elementAt(index)))
                  );
                },
              );
            },
          )
      );
  }
}


class GridMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
              height: 185,
              child:
                GridView.count(
                  padding: EdgeInsets.all(10),
                  childAspectRatio: (MediaQuery.of(context).size.width)/250,
                  crossAxisCount: 3,
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        print('Restaurants Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.RESTAURANT))
                        );
                      },
                      child: Card(
                        elevation: 3,
                        child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(ItemIcons.iconRestaurant, color: HexColors.Primary, size: 30,),
                            Text(Strings.RESTAURANT, style: TextStyle(color: HexColors.PrimaryDark),)
                          ],
                        ),
                      ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Hotels Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.HOTEL))
                        );
                      },
                      child: Card(
                      elevation: 3,
                      child: Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(ItemIcons.iconHotel, color: HexColors.Primary, size: 30,),
                          Text(Strings.HOTEL, style: TextStyle(color: HexColors.PrimaryDark),)
                        ],
                      ),
                      ),
                      ),
                      ),
                    GestureDetector(
                      onTap: () {
                        print('Shops Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.SHOP))
                        );
                      },
                      child: Card(
                      elevation: 3,
                      child: Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(ItemIcons.iconShop, color: HexColors.Primary, size: 30,),
                          Text(Strings.SHOP, style: TextStyle(color: HexColors.PrimaryDark),)
                        ],
                      ),
                    ),
                    ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Beauty Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.BEAUTY))
                        );
                      },
                      child: Card(
                      elevation: 3,
                      child: Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(ItemIcons.iconBeauty, color: HexColors.Primary, size: 30,),
                          Text(Strings.BEAUTY, style: TextStyle(color: HexColors.PrimaryDark),)
                        ],
                      ),
                    ),
                    ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Schools Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.SCHOOL))
                        );
                      },
                      child: Card(
                        elevation: 3,
                        child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(ItemIcons.iconSchool, color: HexColors.Primary, size: 30,),
                            Text(Strings.SCHOOL, style: TextStyle(color: HexColors.PrimaryDark),)
                          ],
                        ),
                      ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Events Pressed!');
                        Navigator.push(context,
                          CupertinoPageRoute(builder: (_context) => ItemList(Strings.EVENT))
                        );
                      },
                      child: Card(
                      elevation: 3,
                      child: Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(ItemIcons.iconEvent, color: HexColors.Primary, size: 30,),
                          Text(Strings.EVENT, style: TextStyle(color: HexColors.PrimaryDark),)
                        ],
                      ),
                    ),
                    ),
                    )
                  ],
                  ),

          );
  }
}


class TopContent extends StatelessWidget {
  const TopContent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15,),
          Text('Top Restaurants in Cebu City', style: TextStyle(fontSize: 15),),
          SizedBox(height: 15,),
          TopList(type: Strings.RESTAURANT,),
          SizedBox(height: 15,),
          Text('Top Hotels in Cebu City', style: TextStyle(fontSize: 15),),
          SizedBox(height: 15,),
          TopList(type: Strings.HOTEL,),
        ],
      ),
    );
  }

}


class TopList extends StatelessWidget {
  const TopList({Key key, this.type}) : super(key: key);

  final String type;

  @override
  Widget build(BuildContext context) {
    final ItemListBloc bloc = BlocProvider.getBloc<ItemListBloc>();

    List<Item> mList;

    return Container(
      height: 120,
      child: StreamBuilder(
      stream: bloc.listStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.data != null) {
          mList = ((snapshot.data) as List<Item>).where((q) => ((q.type == type) && (q.rating > 0)))
                  .toList();
          mList.sort((a,b) => b.rating.compareTo(a.rating));
          return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(mList.length, (index) {
              Item mItem = mList.elementAt(index);
              return TopItem(mItem);
            })
          ),
        );
        }
        else{
          return Container(
            child: Center(
              child: Text('No Data Found'),
            )
          );
        }
      },
    ),
    );
  }
}


class TopItem extends StatelessWidget {
  TopItem(this.mItem);

  final Item mItem;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(builder: (_context) => ItemDetails(mItem))
        );
      },
      child: Container(
      width: 120,
      padding: EdgeInsets.only(right: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 80,
            width: 120,
            // child: Image.network(mItem.image.elementAt(0), fit: BoxFit.cover,),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              imageUrl: mItem.image.elementAt(0),
              placeholder: (context, url) => Container( color: Colors.grey[300], height: 120, width: 80, child: Center(child: Container(width: 20, height: 20, child: CircularProgressIndicator(),),)),
              errorWidget: (context, url, error) => Container( height: 120, width: 80, child: Center(child: Icon(Icons.error),),),
              ),
          ),
          Row(children: <Widget>[
                        Icon((((mItem.rating>1.0)||(mItem.rating==1.0))?Icons.star: ((mItem.rating>0)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:15 ),
                        Icon((((mItem.rating>2.0)||(mItem.rating==2.0))?Icons.star: ((mItem.rating>1)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:15 ),
                        Icon((((mItem.rating>3.0)||(mItem.rating==3.0))?Icons.star: ((mItem.rating>2)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:15 ),
                        Icon((((mItem.rating>4.0)||(mItem.rating==4.0))?Icons.star: ((mItem.rating>3)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:15 ),
                        Icon((((mItem.rating>5.0)||(mItem.rating==5.0))?Icons.star: ((mItem.rating>4)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:15 ),
                        Text('${mItem.rating}')
          ],),
          SizedBox(height: 5,),
          Text(mItem.name, style: TextStyle(fontSize: 12),overflow: TextOverflow.ellipsis)
        ],
      ),
    ),
    );
  }
}