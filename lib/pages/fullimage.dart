import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FullImage extends StatelessWidget {
  final String image;
  FullImage(this.image);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(),
      body: Container(
        color: Colors.black,
        child: Center(
          child: Container(
            child: Image.network(this.image, fit: BoxFit.contain,),
          ),
        ),
      ),
    );
  }
}

class CustomAppbar extends StatelessWidget with PreferredSizeWidget{

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.black,
      leading: IconButton( icon: Icon(CupertinoIcons.back, color: Colors.white,),
        onPressed: () {
          Navigator.of(context).pop();
      },),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(50);
}