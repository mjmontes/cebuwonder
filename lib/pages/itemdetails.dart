import 'package:cebuwonder/helper/constants.dart';
import 'package:cebuwonder/model/item.dart';
import 'package:cebuwonder/pages/fullimage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';

class ItemDetails extends StatelessWidget {
  final Item item;
  ItemDetails(this.item);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(item.name),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (item.video != "")? Video(item.video): Container(height: 10,),
            ImageGallery(item.image),
            PromoContent(item.hasPromo),
            ItemContent(item)
          ],
        ),
      ),
    );
  }
}

class CustomAppbar extends StatelessWidget with PreferredSizeWidget{

  final String title;
  CustomAppbar(this.title);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton( icon: Icon(CupertinoIcons.back),
        onPressed: () {
          Navigator.of(context).pop();
      },),
      title: Text(title, overflow: TextOverflow.ellipsis,),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(50);
}


class Video extends StatelessWidget {
  final String video;
  Video(this.video);

  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      child: YoutubePlayer(
          context: context,
          videoId:YoutubePlayer.convertUrlToId(video),
          flags: YoutubePlayerFlags(
            autoPlay: false,
            showVideoProgressIndicator: true,
          ),
          videoProgressIndicatorColor: Colors.amber,
      ),
    );
  }
}

class PromoContent extends StatelessWidget {
  final bool hasPromo;
  PromoContent(this.hasPromo);

  @override
  Widget build(BuildContext context) {
    print('HASPROMO!!!!!: $hasPromo');
    return (hasPromo)?
    Container(
      margin: EdgeInsets.only(top: 10),
      height: 60,
      width: double.infinity,
      color: Colors.orange,
      child: Center(child: Text('20% Discount', style: TextStyle(fontSize: 25, color: Colors.white)),),
    )
    : Container();
  }
}


class ItemContent extends StatelessWidget {
  final Item item;
  ItemContent(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(22, 5, 22, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10,),
          Text(item.name, style: TextStyle(fontSize: 25),),
          (item.rating>0)? Row(children: <Widget>[
                        Icon((((item.rating>1.0)||(item.rating==1.0))?Icons.star: ((item.rating>0)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                        Icon((((item.rating>2.0)||(item.rating==2.0))?Icons.star: ((item.rating>1)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                        Icon((((item.rating>3.0)||(item.rating==3.0))?Icons.star: ((item.rating>2)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                        Icon((((item.rating>4.0)||(item.rating==4.0))?Icons.star: ((item.rating>3)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                        Icon((((item.rating>5.0)||(item.rating==5.0))?Icons.star: ((item.rating>4)? Icons.star_half: Icons.star_border)), color: Colors.amber, size:20 ),
                        Text('${item.rating}', style: TextStyle(fontSize: 20),)
                      ],)
                      : Container(),
          SizedBox(height: 2,),
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Icon(Icons.location_on),Expanded(child: Text('${item.address}', style: TextStyle(fontSize: 17), maxLines: 3, overflow: TextOverflow.ellipsis,),)],),
          SizedBox(height: 10,),
          Column(children: <Widget>[
            InkWell(
              child:
                Row(children: <Widget>[
                  Icon(Icons.email, color: HexColors.PrimaryDark,),
                  SizedBox(width: 3,),
                  Text('email', style: TextStyle(fontSize: 17, color: Colors.blueGrey),),
                ],),
              onTap: () {
                print('Email Tapped!');
                String subject = 'Enquiry';
                String body = '';
                String url = 'mailto:${item.email.elementAt(0)}?subject=$subject&body=$body%20plugin';
                _launchURL(url);
              },
              ),
            SizedBox(height: 8,),
            InkWell(
              child:
                Row(
                children: <Widget>[
                  Icon(Icons.place, color: HexColors.PrimaryDark,),
                  SizedBox(width: 3,),
                  Text('view in map',style: TextStyle(fontSize: 17, color: Colors.blueGrey))
                ],),
              onTap: () {
                print('Map Tapped!');
                String url  = 'https://www.google.com/maps/search/?api=1&query=${item.coordinates['lat']},${item.coordinates['lng']}&query_place_id=${item.id}';
                _launchURL(url);
              },
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 10),child: Icon(Icons.call, color: HexColors.PrimaryDark,),),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(item.contact.length, (index) {
                    return InkWell(
                      child: Container(padding: EdgeInsets.only(left: 5, top: 10), child: Text(item.contact.elementAt(index), style: TextStyle(fontSize: 18, color: HexColors.PrimaryDark),),),
                      onTap: () {
                        String url = 'tel:${item.contact.elementAt(index)}';
                        _launchURL(url);
                      },
                    );
                  })
                )
              ],
            ),
            ],
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


class ImageGallery extends StatelessWidget {
  final List<String> images;
  ImageGallery(this.images);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: List.generate(images.length, (index) {
          return InkWell(
            child: ImageContent(images.elementAt(index)),
            onTap: () {
              Navigator.push(
                context,
                CupertinoPageRoute(builder: (_context) => FullImage(images.elementAt(index)))
              );
            },
          );
        })
      ),
    );
  }
}

class ImageContent extends StatelessWidget {
  final String image;
  ImageContent(this.image);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 5),
      height: 80,
      width: 120,
      child: CachedNetworkImage(
        fit: BoxFit.fitWidth,
        imageUrl: image,
        placeholder: (context, url) => CircleLoad(),
        errorWidget: (context, url, error) => Container( height: 120, width: 80, child: Center(child: Icon(Icons.error),),),
      ),
    );
  }
}

class CircleLoad extends StatelessWidget {
  const CircleLoad({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}