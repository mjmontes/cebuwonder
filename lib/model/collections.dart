import 'package:flutter/widgets.dart';

class GridMenuItem {

  String title;
  String image;

  GridMenuItem(
    {
      @required this.title,
      @required this.image
    }
  );

}