import 'package:flutter/widgets.dart';


class Item {

  String id;
  String type;
  String video;
  List<String> image;
  String name;
  double rating;
  List<String> contact;
  List<String> email;
  String openingHours;
  String address;
  Map<String, dynamic> coordinates = {
    'lat': 0.0,
    'lng': 0.0
  };
  bool hasPromo;
  bool featured;


  Item(
    {
     @required this.id,
     @required this.type,
     @required this.video,
     @required this.image,
     @required this.name,
     @required this.rating,
     @required this.contact,
     @required this.openingHours,
     @required this.email,
     @required this.address,
     @required this.coordinates,
     @required this.hasPromo,
     @required this.featured
    }
  );

  Item.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    video = json['video'];
    image = json['image'].cast<String>();
    name = json['name'];
    rating = (json['rating'])*1.0;
    contact = json['contact'].cast<String>();
    openingHours = json['openingHours'];
    email = json['email'].cast<String>();
    address = json['address'];
    Map<String, dynamic> mJson= json['coordinates'];
    coordinates['lat'] = mJson['lat'];
    coordinates['lng'] = mJson['lng'];
    hasPromo = json['hasPromo'];
    featured = json['featured'];
  }

}

